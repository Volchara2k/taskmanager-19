package ru.renessans.jvschool.volkov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.service.IAuthenticationService;
import ru.renessans.jvschool.volkov.tm.api.service.IOwnerService;
import ru.renessans.jvschool.volkov.tm.model.Project;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

public final class ProjectDeleteByIndexCommand extends AbstractProjectCommand {

    @NotNull
    private static final String CMD_PROJECT_DELETE_BY_INDEX = "project-delete-by-index";

    @NotNull
    private static final String DESC_PROJECT_DELETE_BY_INDEX = "удалить проект по индексу";

    @NotNull
    private static final String NOTIFY_PROJECT_DELETE_BY_INDEX =
            "Для удаления проекта по индексу введите индекс проекта из списка.\n";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_PROJECT_DELETE_BY_INDEX;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_PROJECT_DELETE_BY_INDEX;
    }

    @Override
    public void execute() {
        ViewUtil.print(NOTIFY_PROJECT_DELETE_BY_INDEX);
        @NotNull final Integer index = ViewUtil.getInteger() - 1;
        @NotNull final IAuthenticationService authService = super.serviceLocator.getAuthenticationService();
        @Nullable final String userId = authService.getUserId();
        @NotNull final IOwnerService<Project> projectService = super.serviceLocator.getProjectService();
        @Nullable final Project project = projectService.deleteByIndex(userId, index);
        ViewUtil.print(project);
    }

}