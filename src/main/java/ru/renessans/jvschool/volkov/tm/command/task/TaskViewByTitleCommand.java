package ru.renessans.jvschool.volkov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.service.IAuthenticationService;
import ru.renessans.jvschool.volkov.tm.api.service.IOwnerService;
import ru.renessans.jvschool.volkov.tm.model.Task;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

public final class TaskViewByTitleCommand extends AbstractTaskCommand {

    @NotNull
    private static final String CMD_TASK_VIEW_BY_TITLE = "task-view-by-title";

    @NotNull
    private static final String DESC_TASK_VIEW_BY_TITLE = "просмотреть задачу по заголовку";

    @NotNull
    private static final String NOTIFY_TASK_VIEW_BY_TITLE =
            "Для отображения задачи по заголовку введите заголовок задачи из списка.\n";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_TASK_VIEW_BY_TITLE;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_TASK_VIEW_BY_TITLE;
    }

    @Override
    public void execute() {
        ViewUtil.print(NOTIFY_TASK_VIEW_BY_TITLE);
        @NotNull final String title = ViewUtil.getLine();
        @Nullable final IAuthenticationService authService = super.serviceLocator.getAuthenticationService();
        @Nullable final String userId = authService.getUserId();
        @NotNull final IOwnerService<Task> taskService = super.serviceLocator.getTaskService();
        @Nullable final Task task = taskService.deleteByTitle(userId, title);
        ViewUtil.print(task);
    }

}