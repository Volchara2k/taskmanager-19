package ru.renessans.jvschool.volkov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.repository.IOwnerRepository;
import ru.renessans.jvschool.volkov.tm.exception.empty.owner.EmptyTaskException;
import ru.renessans.jvschool.volkov.tm.model.Task;

import java.util.*;
import java.util.stream.Collectors;

public final class TaskRepository implements IOwnerRepository<Task> {

    @NotNull
    private final Map<String, Task> taskMap = new LinkedHashMap<>();

    @NotNull
    @Override
    public Task add(
            @NotNull final String userId,
            @NotNull final Task task
    ) {
        task.setUserId(userId);
        this.taskMap.put(task.getId(), task);
        return task;
    }

    @NotNull
    @Override
    public Task removeByIndex(
            @NotNull final String userId,
            @NotNull final Integer index
    ) {
        @Nullable final Task task = getByIndex(userId, index);
        if (Objects.isNull(task)) throw new EmptyTaskException();
        return remove(userId, task);
    }

    @NotNull
    @Override
    public Task removeById(
            @NotNull final String userId,
            @NotNull final String id
    ) {
        @Nullable final Task task = getById(userId, id);
        if (Objects.isNull(task)) throw new EmptyTaskException();
        return remove(userId, task);
    }

    @NotNull
    @Override
    public Task removeByTitle(
            @NotNull final String userId,
            @NotNull final String title
    ) {
        @Nullable final Task task = getByTitle(userId, title);
        if (Objects.isNull(task)) throw new EmptyTaskException();
        return remove(userId, task);
    }

    @NotNull
    @Override
    public Task remove(
            @NotNull final String userId,
            @NotNull final Task task
    ) {
        this.taskMap.remove(task.getId());
        return task;
    }

    @NotNull
    @Override
    public Collection<Task> removeAll(@NotNull final String userId) {
        @Nullable final Collection<Task> removedTasks = getAll(userId);
        this.taskMap.entrySet().removeIf(taskEntry ->
                userId.equals(taskEntry.getValue().getUserId()));
        return removedTasks;
    }

    @Nullable
    @Override
    public Task getByIndex(
            @NotNull final String userId,
            @NotNull final Integer index
    ) {
        @NotNull final List<Task> userTasks = new ArrayList<>(getAll(userId));
        return userTasks.get(index);
    }

    @Nullable
    @Override
    public Task getById(
            @NotNull final String userId,
            @NotNull final String id
    ) {
        return getAll(userId)
                .stream()
                .filter(task -> id.equals(task.getId()))
                .findAny()
                .orElse(null);
    }

    @Nullable
    @Override
    public Task getByTitle(
            @NotNull final String userId,
            @NotNull final String title
    ) {
        return getAll(userId)
                .stream()
                .filter(task -> title.equals(task.getTitle()))
                .findAny()
                .orElse(null);
    }

    @NotNull
    @Override
    public Collection<Task> getAll(@NotNull final String userId) {
        return exportData()
                .stream()
                .filter(task -> userId.equals(task.getUserId()))
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public Collection<Task> importData(@NotNull final Collection<Task> tasks) {
        clearData();
        tasks.forEach(task -> add(task.getUserId(), task));
        return exportData();
    }

    @Override
    public void clearData() {
        this.taskMap.clear();
    }

    @NotNull
    @Override
    public Collection<Task> exportData() {
        return new ArrayList<>(this.taskMap.values());
    }

}