package ru.renessans.jvschool.volkov.tm.command.data.base64;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.service.IDomainService;
import ru.renessans.jvschool.volkov.tm.command.data.AbstractDataCommand;
import ru.renessans.jvschool.volkov.tm.dto.Domain;
import ru.renessans.jvschool.volkov.tm.util.DataInterChangeUtil;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

import java.util.Objects;

public final class DataBase64ImportCommand extends AbstractDataCommand {

    @NotNull
    private static final String CMD_BASE64_IMPORT = "data-base64-import";

    @NotNull
    private static final String DESC_BASE64_IMPORT = "импортировать домен из base64 вида";

    @NotNull
    private static final String NOTIFY_BASE64_IMPORT = "Происходит процесс загрузки домена из base64 вида...";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_BASE64_IMPORT;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return DESC_BASE64_IMPORT;
    }

    @Override
    public void execute() throws Exception {
        ViewUtil.print(NOTIFY_BASE64_IMPORT);

        @Nullable final Domain domain;
        try (@NotNull final DataInterChangeUtil dataInterchange = new DataInterChangeUtil()){
            domain = dataInterchange.readFromBase64(BASE64_FILE_LOCATE, Domain.class);
        }

        if (Objects.isNull(domain)) return;
        @NotNull final IDomainService domainService = super.serviceLocator.getDomainService();
        domainService.dataImport(domain);

        ViewUtil.print(domain.getUsers());
        ViewUtil.print(domain.getTasks());
        ViewUtil.print(domain.getProjects());
    }

}