package ru.renessans.jvschool.volkov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.model.User;

import java.util.Collection;

public interface IUserRepository {

    @Nullable
    User add(@NotNull User user);

    @Nullable
    User getById(@NotNull String id);

    @Nullable
    User getByLogin(@NotNull String login);

    @Nullable
    User deleteById(@NotNull String id);

    @Nullable
    User deleteByLogin(@NotNull String login);

    @Nullable
    User deleteByUser(@NotNull User user);

    @NotNull
    Collection<User> importData(@NotNull Collection<User> users);

    @NotNull
    Collection<User> exportData();

    void clearData();

}