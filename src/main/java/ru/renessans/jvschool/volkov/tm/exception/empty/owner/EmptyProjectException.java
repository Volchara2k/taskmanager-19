package ru.renessans.jvschool.volkov.tm.exception.empty.owner;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.tm.exception.AbstractRuntimeException;

public final class EmptyProjectException extends AbstractRuntimeException {

    @NotNull
    private static final String EMPTY_PROJECT = "Ошибка! Параметр \"проект\" является null!\n";

    public EmptyProjectException() {
        super(EMPTY_PROJECT);
    }

}