package ru.renessans.jvschool.volkov.tm.exception.empty.owner;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.tm.exception.AbstractRuntimeException;

public final class EmptyOwnerModelException extends AbstractRuntimeException {

    @NotNull
    private static final String EMPTY_ABSTRACT_MODEL = "Ошибка! Параметр \"искомая сущность\" является null!\n";

    public EmptyOwnerModelException() {
        super(EMPTY_ABSTRACT_MODEL);
    }

}