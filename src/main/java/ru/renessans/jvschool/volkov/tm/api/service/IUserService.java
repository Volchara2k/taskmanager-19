package ru.renessans.jvschool.volkov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.enumeration.UserRole;
import ru.renessans.jvschool.volkov.tm.model.User;

import java.util.Collection;

public interface IUserService {

    @Nullable
    User getUserById(@Nullable String id);

    @Nullable
    User getUserByLogin(@Nullable String login);

    @Nullable
    User addUser(@Nullable String login, @Nullable String password);

    @Nullable
    User addUser(@Nullable String login, @Nullable String password, @Nullable String firstName);

    @Nullable
    User addUser(@Nullable String login, @Nullable String password, @Nullable UserRole role);

    @NotNull
    User editProfileById(@Nullable String id, @Nullable String firstName);

    @NotNull
    User editProfileById(@Nullable String id, @Nullable String firstName, @Nullable String lastName);

    @NotNull
    User updatePasswordById(@Nullable String id, @Nullable String newPassword);

    @NotNull
    User lockUserByLogin(@Nullable String login);

    @NotNull
    User unlockUserByLogin(@Nullable String login);

    @Nullable
    User deleteUserById(@Nullable String id);

    @Nullable
    User deleteUserByLogin(@Nullable String login);

    @Nullable
    User deleteByUser(@Nullable User user);

    @NotNull
    Collection<User> initDemoData();

    @Nullable
    Collection<User> importData(@Nullable Collection<User> userList);

    @NotNull
    Collection<User> getExportData();

}