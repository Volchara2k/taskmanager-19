package ru.renessans.jvschool.volkov.tm.exception.empty.command;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.tm.exception.AbstractRuntimeException;

public final class EmptyCommandException extends AbstractRuntimeException {

    @NotNull
    private static final String EMPTY_COMMAND = "Ошибка! Параметр \"команда\" является пустым или null!\n";

    public EmptyCommandException() {
        super(EMPTY_COMMAND);
    }

}