package ru.renessans.jvschool.volkov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.repository.IAuthenticationRepository;
import ru.renessans.jvschool.volkov.tm.model.User;

public final class AuthenticationRepository implements IAuthenticationRepository {

    @Nullable
    private String userId;

    @Nullable
    @Override
    public String getUserId() {
        return this.userId;
    }

    @Override
    public void subscribe(@NotNull final User user) {
        this.userId = user.getId();
    }

    @Override
    public boolean unsubscribe() {
        this.userId = null;
        return true;
    }

}