package ru.renessans.jvschool.volkov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.model.User;

import java.util.Collection;

public interface IOwnerService<T> {

    @NotNull
    T add(@Nullable String userId, @Nullable String title, @Nullable String description);

    @NotNull
    T updateByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String title, @Nullable String description);

    @NotNull
    T updateById(@Nullable String userId, @Nullable String id, @Nullable String title, @Nullable String description);

    @NotNull
    T deleteByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull
    T deleteById(@Nullable String userId, @Nullable String id);

    @NotNull
    T deleteByTitle(@Nullable String userId, @Nullable String title);

    @NotNull
    Collection<T> deleteAll(@Nullable String userId);

    @Nullable
    T getByIndex(@Nullable String userId, @Nullable Integer index);

    @Nullable
    T getById(@Nullable String userId, @Nullable String id);

    @Nullable
    T getByTitle(@Nullable String userId, @Nullable String title);

    @NotNull
    Collection<T> getAll(@Nullable String userId);

    @NotNull
    Collection<T> initDemoData(@Nullable Collection<User> users);

    @Nullable
    Collection<T> importData(@Nullable Collection<T> objects);

    @NotNull
    Collection<T> exportData();

}