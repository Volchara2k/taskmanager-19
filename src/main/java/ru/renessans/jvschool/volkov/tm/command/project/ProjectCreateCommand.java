package ru.renessans.jvschool.volkov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.service.IAuthenticationService;
import ru.renessans.jvschool.volkov.tm.api.service.IOwnerService;
import ru.renessans.jvschool.volkov.tm.model.Project;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

public final class ProjectCreateCommand extends AbstractProjectCommand {

    @NotNull
    private static final String CMD_PROJECT_CREATE = "project-create";

    @NotNull
    private static final String DESC_PROJECT_CREATE = "добавить новый проект";

    @NotNull
    private static final String NOTIFY_PROJECT_CREATE =
            "Для создания проекта введите его заголовок или заголовок с описанием.\n";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_PROJECT_CREATE;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_PROJECT_CREATE;
    }

    @Override
    public void execute() {
        ViewUtil.print(NOTIFY_PROJECT_CREATE);
        @NotNull final String title = ViewUtil.getLine();
        @NotNull final String description = ViewUtil.getLine();
        @NotNull final IAuthenticationService authService = super.serviceLocator.getAuthenticationService();
        @Nullable final String userId = authService.getUserId();
        @NotNull final IOwnerService<Project> projectService = super.serviceLocator.getProjectService();
        @Nullable final Project project = projectService.add(userId, title, description);
        ViewUtil.print(project);
    }

}