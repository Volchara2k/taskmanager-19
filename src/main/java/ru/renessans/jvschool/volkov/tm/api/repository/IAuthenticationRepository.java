package ru.renessans.jvschool.volkov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.model.User;

public interface IAuthenticationRepository {

    @Nullable
    String getUserId();

    void subscribe(@NotNull User user);

    boolean unsubscribe();

}