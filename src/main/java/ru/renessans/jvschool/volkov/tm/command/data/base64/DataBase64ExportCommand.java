package ru.renessans.jvschool.volkov.tm.command.data.base64;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.service.IDomainService;
import ru.renessans.jvschool.volkov.tm.command.data.AbstractDataCommand;
import ru.renessans.jvschool.volkov.tm.dto.Domain;
import ru.renessans.jvschool.volkov.tm.util.DataInterChangeUtil;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

public final class DataBase64ExportCommand extends AbstractDataCommand {

    @NotNull
    private static final String CMD_BASE64_EXPORT = "data-base64-export";

    @NotNull
    private static final String DESC_BASE64_EXPORT = "экспортировать домен в base64 вид";

    @NotNull
    private static final String NOTIFY_BASE64_EXPORT = "Происходит процесс выгрузки домена в base64 вид...";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_BASE64_EXPORT;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_BASE64_EXPORT;
    }

    @Override
    public void execute() throws Exception {
        ViewUtil.print(NOTIFY_BASE64_EXPORT);

        @NotNull final Domain domain = new Domain();
        @NotNull final IDomainService domainService = serviceLocator.getDomainService();
        domainService.dataExport(domain);

        try (@Nullable final DataInterChangeUtil dataInterchange = new DataInterChangeUtil()){
            dataInterchange.writeToBase64(domain, BASE64_FILE_LOCATE);
        }

        ViewUtil.print(domain.getUsers());
        ViewUtil.print(domain.getTasks());
        ViewUtil.print(domain.getProjects());
    }

}
