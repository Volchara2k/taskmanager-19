package ru.renessans.jvschool.volkov.tm.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.exception.empty.data.EmptyFileException;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

public final class DataInterChangeUtil implements Closeable {

    @Nullable
    private ObjectInputStream objectInputStream;

    @Nullable
    private ObjectOutputStream objectOutputStream;

    @Nullable
    private ByteArrayInputStream byteArrayInputStream;

    @Nullable
    private ByteArrayOutputStream byteArrayOutputStream;

    @Nullable
    private FileInputStream fileInputStream;

    @Nullable
    private FileOutputStream fileOutputStream;

    @Override
    public void close() throws IOException {
        if (!Objects.isNull(this.objectOutputStream)) this.objectOutputStream.close();
        if (!Objects.isNull(this.objectInputStream)) this.objectInputStream.close();
        if (!Objects.isNull(this.byteArrayOutputStream)) this.byteArrayOutputStream.close();
        if (!Objects.isNull(this.byteArrayInputStream)) this.byteArrayInputStream.close();
        if (!Objects.isNull(this.fileOutputStream)) this.fileOutputStream.close();
        if (!Objects.isNull(this.fileInputStream)) this.fileInputStream.close();
    }

    @SneakyThrows
    public <T extends Serializable> void writeToBin(
            @NotNull final T t,
            @NotNull final String filename
    ) {
        @NotNull final File file = FileUtil.createFile(filename);
        this.fileOutputStream = new FileOutputStream(file);
        writeAsFile(t, fileOutputStream);
    }

    @Nullable
    @SneakyThrows
    public <T extends Serializable> T readFromBin(
            @NotNull final String filename,
            @NotNull final Class<T> tClass
    ) {
        if (FileUtil.fileIsNotExists(filename)) throw new EmptyFileException();
        this.fileInputStream = new FileInputStream(filename);
        return readObject(fileInputStream, tClass);
    }

    @SneakyThrows
    public <T extends Serializable> void writeToBase64(
            @NotNull final T t,
            @NotNull final String filename
    ) {
        this.byteArrayOutputStream = new ByteArrayOutputStream();
        writeAsFile(t, byteArrayOutputStream);

        final byte[] fileBytes = byteArrayOutputStream.toByteArray();
        @NotNull final String base64 = new BASE64Encoder().encode(fileBytes);
        final byte[] base64Bytes = base64.getBytes(StandardCharsets.UTF_8);

        writeAsFile(base64Bytes, filename);
    }

    @Nullable
    @SneakyThrows
    public <T extends Serializable> T readFromBase64(
            @NotNull final String filename,
            @NotNull final Class<T> tClass
    ) {
        if (FileUtil.fileIsNotExists(filename)) throw new EmptyFileException();

        final byte[] fileBytes = FileUtil.readFile(filename);
        @NotNull final String base64 = new String(fileBytes);
        final byte[] base64Bytes = new BASE64Decoder().decodeBuffer(base64);

        this.byteArrayInputStream = new ByteArrayInputStream(base64Bytes);
        return readObject(this.byteArrayInputStream, tClass);
    }

    @SneakyThrows
    public <T> void writeToJson(
            @NotNull final T t,
            @NotNull final String filename
    ) {
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        writeAsFile(t, objectMapper, filename);
    }

    @Nullable
    @SneakyThrows
    public <T> T readFromJson(
            @NotNull final String filename,
            @NotNull final Class<T> tClass
    ) {
        if (FileUtil.fileIsNotExists(filename)) throw new EmptyFileException();
        return readObject(filename, tClass);
    }

    @SneakyThrows
    public <T> void writeToXml(
            @NotNull final T t,
            @NotNull final String filename
    ) {
        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        writeAsFile(t, objectMapper, filename);
    }

    @Nullable
    @SneakyThrows
    public <T> T readFromXml(
            @NotNull final String filename,
            @NotNull final Class<T> tClass
    ) {
        if (FileUtil.fileIsNotExists(filename)) throw new EmptyFileException();
        return readObject(filename, tClass);
    }

    @SneakyThrows
    public <T> void writeToYaml(
            @NotNull final T t,
            @NotNull final String filename
    ) {
        @NotNull final YAMLFactory yamlFactory = new YAMLFactory();
        @NotNull final ObjectMapper objectMapper = new YAMLMapper(yamlFactory);
        writeAsFile(t, objectMapper, filename);
    }

    @Nullable
    @SneakyThrows
    public <T> T readFromYaml(
            @NotNull final String filename,
            @NotNull final Class<T> tClass
    ) {
        if (FileUtil.fileIsNotExists(filename)) throw new EmptyFileException();
        return readObject(filename, tClass);
    }

    @SneakyThrows
    private <T> T readObject(
            @NotNull final InputStream inputStream,
            @NotNull final Class<T> tClass
    ) {
        this.objectInputStream = new ObjectInputStream(inputStream);
        return tClass.cast(this.objectInputStream.readObject());
    }

    @Nullable
    @SneakyThrows
    private <T> T readObject(
            @NotNull final String filename,
            @NotNull final Class<T> tClass
    ) {
        this.fileInputStream = new FileInputStream(filename);
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(fileInputStream, tClass);
    }

    @SneakyThrows
    private <T> void writeAsFile(
            @NotNull final T t,
            @NotNull final OutputStream outputStream
    ) {
        this.objectOutputStream = new ObjectOutputStream(outputStream);
        this.objectOutputStream.writeObject(t);
        this.objectOutputStream.flush();
    }

    @SneakyThrows
    private <T> void writeAsFile(
            @NotNull final T t,
            @NotNull final ObjectMapper objectMapper,
            @NotNull final String filename
    ) {
        @NotNull final File file = FileUtil.createFile(filename);
        objectMapper.writerWithDefaultPrettyPrinter().writeValue(file, t);
    }

    @SneakyThrows
    private void writeAsFile(
            final byte[] bytes,
            @NotNull final String filename
    ) {
        @NotNull final File file = FileUtil.createFile(filename);
        this.fileOutputStream = new FileOutputStream(file.getName());
        this.fileOutputStream.write(bytes);
        this.fileOutputStream.flush();
    }

}