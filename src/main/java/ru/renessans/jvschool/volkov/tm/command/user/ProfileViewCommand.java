package ru.renessans.jvschool.volkov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.service.IAuthenticationService;
import ru.renessans.jvschool.volkov.tm.api.service.IUserService;
import ru.renessans.jvschool.volkov.tm.model.User;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

public final class ProfileViewCommand extends AbstractAuthCommand {

    @NotNull
    private static final String CMD_VIEW_PROFILE = "view-profile";

    @NotNull
    private static final String DESC_VIEW_PROFILE = "обновить пароль пользователя";

    @NotNull
    private static final String NOTIFY_VIEW_PROFILE = "Информация о текущем профиле пользователя: \n";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_VIEW_PROFILE;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_VIEW_PROFILE;
    }

    @Override
    public void execute() {
        ViewUtil.print(NOTIFY_VIEW_PROFILE);
        @NotNull final IAuthenticationService authService = super.serviceLocator.getAuthenticationService();
        @Nullable final String userId = authService.getUserId();
        @NotNull final IUserService userService = super.serviceLocator.getUserService();
        @Nullable final User user = userService.getUserById(userId);
        ViewUtil.print(user);
    }

}