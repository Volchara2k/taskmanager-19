package ru.renessans.jvschool.volkov.tm.command.data.yaml;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.service.IDomainService;
import ru.renessans.jvschool.volkov.tm.command.data.AbstractDataCommand;
import ru.renessans.jvschool.volkov.tm.dto.Domain;
import ru.renessans.jvschool.volkov.tm.util.DataInterChangeUtil;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

public final class DataYamlExportCommand extends AbstractDataCommand {

    @NotNull
    private static final String CMD_YAML_EXPORT = "data-yaml-export";

    @NotNull
    private static final String DESC_YAML_EXPORT = "экспортировать домен в yaml вид";

    @NotNull
    private static final String NOTIFY_YAML_EXPORT = "Происходит процесс выгрузки домена в yaml вид...";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_YAML_EXPORT;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_YAML_EXPORT;
    }

    @Override
    public void execute() throws Exception {
        ViewUtil.print(NOTIFY_YAML_EXPORT);

        @NotNull final Domain domain = new Domain();
        @NotNull final IDomainService domainService = serviceLocator.getDomainService();
        domainService.dataExport(domain);

        try (@NotNull final DataInterChangeUtil dataInterchange = new DataInterChangeUtil()) {
            dataInterchange.writeToYaml(domain, YAML_FILE_LOCATE);
        }

        ViewUtil.print(domain.getUsers());
        ViewUtil.print(domain.getTasks());
        ViewUtil.print(domain.getProjects());
    }

}