package ru.renessans.jvschool.volkov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.repository.IUserRepository;
import ru.renessans.jvschool.volkov.tm.exception.empty.user.EmptyUserException;
import ru.renessans.jvschool.volkov.tm.model.User;

import java.util.*;

public final class UserRepository implements IUserRepository {

    @NotNull
    private final Map<String, User> userMap = new LinkedHashMap<>();

    @NotNull
    @Override
    public User add(@NotNull final User user) {
        this.userMap.put(user.getId(), user);
        return user;
    }

    @Nullable
    @Override
    public User getById(@NotNull final String id) {
        return exportData()
                .stream()
                .filter(user -> id.equals(user.getId()))
                .findAny()
                .orElse(null);
    }

    @Nullable
    @Override
    public User getByLogin(@NotNull final String login) {
        return exportData()
                .stream()
                .filter(user -> login.equals(user.getLogin()))
                .findAny()
                .orElse(null);
    }

    @NotNull
    @Override
    public User deleteById(@NotNull final String id) {
        @Nullable final User user = getById(id);
        if (Objects.isNull(user)) throw new EmptyUserException();
        return deleteByUser(user);
    }

    @NotNull
    @Override
    public User deleteByLogin(@NotNull final String login) {
        @Nullable final User user = getByLogin(login);
        if (Objects.isNull(user)) throw new EmptyUserException();
        return deleteByUser(user);
    }

    @NotNull
    @Override
    public User deleteByUser(@NotNull final User user) {
        this.userMap.values().remove(user);
        return user;
    }

    @NotNull
    @Override
    public Collection<User> importData(@NotNull final Collection<User> users) {
        clearData();
        users.forEach(this::add);
        return exportData();
    }

    @NotNull
    @Override
    public Collection<User> exportData() {
        return new ArrayList<>(this.userMap.values());
    }

    @Override
    public void clearData() {
        this.userMap.clear();
    }

}