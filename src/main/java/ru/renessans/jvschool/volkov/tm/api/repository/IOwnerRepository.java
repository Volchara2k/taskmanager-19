package ru.renessans.jvschool.volkov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public interface IOwnerRepository<T> {

    @NotNull
    T add(@NotNull String userId, @NotNull T object);

    @NotNull
    T removeByIndex(@NotNull String userId, @NotNull Integer index);

    @NotNull
    T removeById(@NotNull String userId, @NotNull String id);

    @NotNull
    T removeByTitle(@NotNull String userId, @NotNull String title);

    @NotNull
    T remove(@NotNull String userId, @NotNull T object);

    @NotNull
    Collection<T> removeAll(@NotNull String userId);

    @Nullable
    T getByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    T getById(@NotNull String userId, @NotNull String id);

    @Nullable
    T getByTitle(@NotNull String userId, @NotNull String title);

    @NotNull
    Collection<T> getAll(@NotNull String userId);

    @Nullable
    Collection<T> importData(@NotNull Collection<T> objects);

    @NotNull
    Collection<T> exportData();

    void clearData();

}