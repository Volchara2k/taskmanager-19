package ru.renessans.jvschool.volkov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.tm.command.AbstractCommand;
import ru.renessans.jvschool.volkov.tm.enumeration.UserRole;

public abstract class AbstractAuthCommand extends AbstractCommand {

    @NotNull
    @Override
    public UserRole[] permissions() {
        return new UserRole[]{UserRole.ADMIN, UserRole.USER};
    }

}