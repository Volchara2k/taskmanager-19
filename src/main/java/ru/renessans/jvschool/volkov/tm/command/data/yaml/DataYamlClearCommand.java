package ru.renessans.jvschool.volkov.tm.command.data.yaml;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.command.data.AbstractDataCommand;
import ru.renessans.jvschool.volkov.tm.util.FileUtil;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

public final class DataYamlClearCommand extends AbstractDataCommand {

    @NotNull
    private static final String CMD_YAML_CLEAR = "data-yaml-clear";

    @NotNull
    private static final String DESC_YAML_CLEAR = "очистить yaml данные";

    @NotNull
    private static final String NOTIFY_YAML_CLEAR = "Происходит процесс очищения yaml данных...";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_YAML_CLEAR;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_YAML_CLEAR;
    }

    @Override
    public void execute() throws Exception {
        ViewUtil.print(NOTIFY_YAML_CLEAR);
        final boolean removeState = FileUtil.deleteFile(YAML_FILE_LOCATE);
        ViewUtil.print(removeState);
    }

}