package ru.renessans.jvschool.volkov.tm.enumeration;

import org.jetbrains.annotations.NotNull;

public enum AuthState {

    @NotNull
    SUCCESS("Успешно!"),

    @NotNull
    USER_NOT_FOUND("Пользователь не найден!"),

    @NotNull
    INVALID_PASSWORD("Некорректный пароль!"),

    @NotNull
    NO_ACCESS_RIGHTS("Нет прав доступа!"),

    @NotNull
    NEED_LOG_IN("Необходимо авторизоваться!"),

    @NotNull
    LOCKDOWN_PROFILE("Профиль заблокирован!");

    @NotNull
    private final String title;

    AuthState(@NotNull final String title) {
        this.title = title;
    }

    @NotNull
    public String getTitle() {
        return this.title;
    }

    public boolean isSuccess() {
        return this == SUCCESS;
    }

}