package ru.renessans.jvschool.volkov.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.enumeration.AuthState;
import ru.renessans.jvschool.volkov.tm.enumeration.UserRole;
import ru.renessans.jvschool.volkov.tm.model.User;

public interface IAuthenticationService {

    @Nullable
    String getUserId();

    @Nullable
    UserRole getUserRole();

    void verifyPermissions(@Nullable UserRole[] userRoles);

    @Nullable
    AuthState signIn(@Nullable String login, @Nullable String password);

    @Nullable
    User signUp(@Nullable String login, @Nullable String password);

    @Nullable
    User signUp(@Nullable String login, @Nullable String password, @Nullable String email);

    @Nullable
    User signUp(@Nullable String login, @Nullable String password, @Nullable UserRole role);

    boolean logOut();

}