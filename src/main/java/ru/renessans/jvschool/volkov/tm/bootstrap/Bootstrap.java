package ru.renessans.jvschool.volkov.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.repository.IAuthenticationRepository;
import ru.renessans.jvschool.volkov.tm.api.repository.ICommandRepository;
import ru.renessans.jvschool.volkov.tm.api.repository.IOwnerRepository;
import ru.renessans.jvschool.volkov.tm.api.repository.IUserRepository;
import ru.renessans.jvschool.volkov.tm.api.service.*;
import ru.renessans.jvschool.volkov.tm.command.AbstractCommand;
import ru.renessans.jvschool.volkov.tm.constant.DataConst;
import ru.renessans.jvschool.volkov.tm.exception.unknown.UnknownCommandException;
import ru.renessans.jvschool.volkov.tm.model.Project;
import ru.renessans.jvschool.volkov.tm.model.Task;
import ru.renessans.jvschool.volkov.tm.model.User;
import ru.renessans.jvschool.volkov.tm.repository.*;
import ru.renessans.jvschool.volkov.tm.service.*;
import ru.renessans.jvschool.volkov.tm.util.ScannerUtil;
import ru.renessans.jvschool.volkov.tm.util.ValidRuleUtil;

import java.util.Collection;
import java.util.Objects;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IUserService userService = new UserService(userRepository);


    @NotNull
    private final IAuthenticationRepository authRepository = new AuthenticationRepository();

    @NotNull
    private final IAuthenticationService authService = new AuthenticationService(authRepository, userService);


    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);


    @NotNull
    private final IOwnerRepository<Task> taskRepository = new TaskRepository();

    @NotNull
    private final IOwnerService<Task> taskService = new TaskService(taskRepository);


    @NotNull
    private final IOwnerRepository<Project> projectRepository = new ProjectRepository();

    @NotNull
    private final IOwnerService<Project> projectService = new ProjectService(projectRepository);


    @NotNull
    private final IDomainService domainService = new DomainService(
            userService, taskService, projectService
    );

    {
        commandService.initCommands(this);
        @NotNull final Collection<User> users = userService.initDemoData();
        taskService.initDemoData(users);
        projectService.initDemoData(users);
    }

    @NotNull
    @Override
    public IUserService getUserService() {
        return this.userService;
    }

    @NotNull
    @Override
    public IAuthenticationService getAuthenticationService() {
        return this.authService;
    }

    @NotNull
    @Override
    public IOwnerService<Task> getTaskService() {
        return this.taskService;
    }

    @NotNull
    @Override
    public IOwnerService<Project> getProjectService() {
        return this.projectService;
    }

    @NotNull
    @Override
    public ICommandService getCommandService() {
        return this.commandService;
    }

    @NotNull
    @Override
    public IDomainService getDomainService() {
        return domainService;
    }

    public void run(@Nullable final String... args) {
        final boolean emptyArgs = ValidRuleUtil.isNullOrEmpty(args);
        if (emptyArgs) terminalCommandPrintLoop();
        else argumentPrint(Objects.requireNonNull(args[0]));
    }

    private void terminalCommandPrintLoop() {
        @NotNull String command = "";
        while (!DataConst.EXIT_FACTOR.equals(command)) {
            try {
                command = ScannerUtil.getLine();
                @Nullable final AbstractCommand abstractCommand = this.commandService.getTerminalCommand(command);

                if (Objects.isNull(abstractCommand)) throw new UnknownCommandException(command);
                authService.verifyPermissions(abstractCommand.permissions());

                abstractCommand.execute();
            } catch (final Exception e) {
                System.err.print(e.getMessage());
            }
        }
    }

    private void argumentPrint(@NotNull final String arg) {
        try {
            @Nullable final AbstractCommand abstractCommand = this.commandService.getArgumentCommand(arg);

            if (Objects.isNull(abstractCommand)) throw new UnknownCommandException(arg);
            authService.verifyPermissions(abstractCommand.permissions());

            abstractCommand.execute();
        } catch (final Exception e) {
            System.err.print(e.getMessage());
        }
    }

}