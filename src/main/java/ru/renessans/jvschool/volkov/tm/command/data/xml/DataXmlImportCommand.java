package ru.renessans.jvschool.volkov.tm.command.data.xml;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.service.IDomainService;
import ru.renessans.jvschool.volkov.tm.command.data.AbstractDataCommand;
import ru.renessans.jvschool.volkov.tm.dto.Domain;
import ru.renessans.jvschool.volkov.tm.util.DataInterChangeUtil;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

import java.util.Objects;

public final class DataXmlImportCommand extends AbstractDataCommand {

    @NotNull
    private static final String CMD_XML_IMPORT = "data-xml-import";

    @NotNull
    private static final String DESC_XML_IMPORT = "импортировать домен из xml вида";

    @NotNull
    private static final String NOTIFY_XML_IMPORT = "Происходит процесс загрузки домена из xml вида...";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_XML_IMPORT;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_XML_IMPORT;
    }

    @Override
    public void execute() throws Exception {
        ViewUtil.print(NOTIFY_XML_IMPORT);

        @Nullable final Domain domain;
        try (@NotNull final DataInterChangeUtil dataInterchange = new DataInterChangeUtil()) {
            domain = dataInterchange.readFromXml(XML_FILE_LOCATE, Domain.class);
        }

        if (Objects.isNull(domain)) return;
        @NotNull final IDomainService domainService = super.serviceLocator.getDomainService();
        domainService.dataImport(domain);

        ViewUtil.print(domain.getUsers());
        ViewUtil.print(domain.getTasks());
        ViewUtil.print(domain.getProjects());
    }

}