package ru.renessans.jvschool.volkov.tm.util;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.enumeration.AuthState;
import ru.renessans.jvschool.volkov.tm.exception.empty.owner.EmptyOwnerModelException;
import ru.renessans.jvschool.volkov.tm.exception.security.AccessFailureException;
import ru.renessans.jvschool.volkov.tm.exception.illegal.IllegalProcessCompleting;
import ru.renessans.jvschool.volkov.tm.model.AbstractSerializableModel;

import java.util.Collection;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

@UtilityClass
public final class ViewUtil {

    @NotNull
    private static final String SUCCESSFUL_OUTCOME = "Операция УСПЕШНО завершилась!\n";

    @NotNull
    private static final String UNSUCCESSFUL_OUTCOME = "Операция завершилась с ОШИБКОЙ!\n";

    public void print(@NotNull final String string) {
        System.out.println(string);
    }

    public void print(final boolean state) {
        if (!state) {
            print(UNSUCCESSFUL_OUTCOME);
            throw new IllegalProcessCompleting();
        }

        print(SUCCESSFUL_OUTCOME);
    }

    public void print(@Nullable final Collection<?> collection) {
        if (ValidRuleUtil.isNullOrEmpty(collection)) {
            print("Список на текущий момент пуст.");
            return;
        }

        @NotNull final AtomicInteger index = new AtomicInteger(1);
        collection.forEach(element -> {
            print(index + ". " + element);
            index.getAndIncrement();
        });

        print(SUCCESSFUL_OUTCOME);
    }

    public void print(@Nullable final AbstractSerializableModel model) {
        if (Objects.isNull(model)) {
            print(UNSUCCESSFUL_OUTCOME);
            throw new EmptyOwnerModelException();
        }

        print(model.toString());
    }

    public void print(@Nullable final AuthState authState) {
        if (Objects.isNull(authState)) {
            print(UNSUCCESSFUL_OUTCOME);
            throw new AccessFailureException();
        }

        if (!authState.isSuccess()) {
            print(UNSUCCESSFUL_OUTCOME);
            throw new AccessFailureException(authState.getTitle());
        }

        print(SUCCESSFUL_OUTCOME);
    }

    @NotNull
    public String getLine() {
        System.out.print("Введите данные: ");
        return ScannerUtil.getLine();
    }

    @NotNull
    public Integer getInteger() {
        System.out.print("Введите данные: ");
        return ScannerUtil.getInteger();
    }

}