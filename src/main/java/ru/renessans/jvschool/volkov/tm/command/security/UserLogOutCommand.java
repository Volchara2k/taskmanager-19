package ru.renessans.jvschool.volkov.tm.command.security;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.service.IAuthenticationService;
import ru.renessans.jvschool.volkov.tm.command.AbstractCommand;
import ru.renessans.jvschool.volkov.tm.enumeration.UserRole;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

public final class UserLogOutCommand extends AbstractCommand {

    @NotNull
    private static final String CMD_LOG_OUT = "log-out";

    @Nullable
    private static final String DESC_LOG_OUT = "выйти из системы";

    @NotNull
    private static final String NOTIFY_LOG_OUT = "Производится выход пользователя из системы...";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_LOG_OUT;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @Nullable
    @Override
    public UserRole[] permissions() {
        return new UserRole[]{UserRole.ADMIN, UserRole.USER};
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_LOG_OUT;
    }

    @Override
    public void execute() {
        ViewUtil.print(NOTIFY_LOG_OUT);
        @NotNull final IAuthenticationService authService = super.serviceLocator.getAuthenticationService();
        final boolean logout = authService.logOut();
        ViewUtil.print(logout);
    }

}