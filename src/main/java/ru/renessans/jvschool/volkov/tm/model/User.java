package ru.renessans.jvschool.volkov.tm.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.enumeration.UserRole;
import ru.renessans.jvschool.volkov.tm.util.ValidRuleUtil;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public final class User extends AbstractSerializableModel {

    private static final long serialVersionUID = 1L;

    @NotNull
    private String login = "";

    @NotNull
    private String passwordHash = "";

    @Nullable
    private String firstName = "";

    @Nullable
    private String lastName = "";

    @Nullable
    private String middleName = "";

    @NotNull
    private UserRole role = UserRole.USER;

    @NotNull
    private Boolean lockdown = false;

    public User(
            @NotNull final String login,
            @NotNull final String passwordHash
    ) {
        this.login = login;
        this.passwordHash = passwordHash;
    }

    public User(
            @NotNull final String login,
            @NotNull final String password,
            @NotNull final UserRole role
    ) {
        this.login = login;
        this.passwordHash = password;
        this.role = role;
    }

    public User(
            @NotNull final String login,
            @NotNull final String password,
            @NotNull final String firstName
    ) {
        this.login = login;
        this.passwordHash = password;
        this.firstName = firstName;
    }

    @NotNull
    @Override
    public String toString() {
        @NotNull final StringBuilder result = new StringBuilder();
        if (!ValidRuleUtil.isNullOrEmpty(this.login))
            result.append("Логин: ").append(login);
        if (!ValidRuleUtil.isNullOrEmpty(this.firstName))
            result.append(", имя: ").append(this.firstName).append("\n");
        if (!ValidRuleUtil.isNullOrEmpty(this.lastName))
            result.append(", фамилия: ").append(this.lastName).append("\n");
        result.append("\nРоль: ").append(this.role.getTitle()).append("\n");
        result.append("\nИдентификатор: ").append(super.getId()).append("\n");
        return result.toString();
    }

}