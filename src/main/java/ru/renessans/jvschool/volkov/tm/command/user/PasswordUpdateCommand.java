package ru.renessans.jvschool.volkov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.service.IAuthenticationService;
import ru.renessans.jvschool.volkov.tm.api.service.IUserService;
import ru.renessans.jvschool.volkov.tm.model.User;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

public final class PasswordUpdateCommand extends AbstractAuthCommand {

    @NotNull
    private static final String CMD_UPDATE_PASSWORD = "update-password";

    @NotNull
    private static final String ARG_UPDATE_PASSWORD = "обновить пароль пользователя";

    @NotNull
    private static final String NOTIFY_UPDATE_PASSWORD = "Для смены пароля введите новый пароль: \n";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_UPDATE_PASSWORD;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return ARG_UPDATE_PASSWORD;
    }

    @Override
    public void execute() {
        ViewUtil.print(NOTIFY_UPDATE_PASSWORD);
        @NotNull final String password = ViewUtil.getLine();
        @NotNull final IAuthenticationService authService = super.serviceLocator.getAuthenticationService();
        @Nullable final String userId = authService.getUserId();
        @NotNull final IUserService userService = super.serviceLocator.getUserService();
        @Nullable final User user = userService.updatePasswordById(userId, password);
        ViewUtil.print(user);
    }

}