package ru.renessans.jvschool.volkov.tm.exception.empty.data;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.tm.exception.AbstractRuntimeException;

public final class EmptyFileException extends AbstractRuntimeException {

    @NotNull
    private static final String EMPTY_FILE =
            "Ошибка! Параметр \"файл\" является пустым, null, или не существует в системе!\n";

    public EmptyFileException() {
        super(EMPTY_FILE);
    }

}