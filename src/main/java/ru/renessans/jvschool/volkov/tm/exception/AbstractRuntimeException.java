package ru.renessans.jvschool.volkov.tm.exception;

import org.jetbrains.annotations.NotNull;

public abstract class AbstractRuntimeException extends RuntimeException {

    public AbstractRuntimeException(@NotNull final String message) {
        super(message);
    }

    public AbstractRuntimeException(@NotNull final Throwable cause) {
        super(cause);
    }

}