package ru.renessans.jvschool.volkov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.tm.model.Project;
import ru.renessans.jvschool.volkov.tm.model.Task;

public interface IServiceLocator {

    @NotNull
    IUserService getUserService();

    @NotNull
    IAuthenticationService getAuthenticationService();

    @NotNull
    IOwnerService<Task> getTaskService();

    @NotNull
    IOwnerService<Project> getProjectService();

    @NotNull
    ICommandService getCommandService();

    @NotNull
    IDomainService getDomainService();

}