package ru.renessans.jvschool.volkov.tm.service;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.repository.IOwnerRepository;
import ru.renessans.jvschool.volkov.tm.api.service.IOwnerService;
import ru.renessans.jvschool.volkov.tm.constant.DataConst;
import ru.renessans.jvschool.volkov.tm.exception.empty.owner.EmptyDescriptionException;
import ru.renessans.jvschool.volkov.tm.exception.empty.owner.EmptyIdException;
import ru.renessans.jvschool.volkov.tm.exception.empty.owner.EmptyTaskException;
import ru.renessans.jvschool.volkov.tm.exception.empty.owner.EmptyTitleException;
import ru.renessans.jvschool.volkov.tm.exception.empty.user.EmptyUserException;
import ru.renessans.jvschool.volkov.tm.exception.empty.user.EmptyUserIdException;
import ru.renessans.jvschool.volkov.tm.exception.illegal.IllegalIndexException;
import ru.renessans.jvschool.volkov.tm.model.Task;
import ru.renessans.jvschool.volkov.tm.model.User;
import ru.renessans.jvschool.volkov.tm.util.ValidRuleUtil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

@AllArgsConstructor
public final class TaskService implements IOwnerService<Task> {

    @NotNull
    private final IOwnerRepository<Task> taskRepository;

    @NotNull
    @Override
    public Task add(
            @Nullable final String userId,
            @Nullable final String title,
            @Nullable final String description
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(title)) throw new EmptyTitleException();
        if (ValidRuleUtil.isNullOrEmpty(description)) throw new EmptyDescriptionException();

        @NotNull final Task task = new Task(title, description);
        return this.taskRepository.add(userId, task);
    }

    @NotNull
    @Override
    public Task updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String title,
            @Nullable final String description
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(index)) throw new IllegalIndexException();
        if (ValidRuleUtil.isNullOrEmpty(title)) throw new EmptyTitleException();
        if (ValidRuleUtil.isNullOrEmpty(description)) throw new EmptyDescriptionException();

        @Nullable final Task task = getByIndex(userId, index);
        if (Objects.isNull(task)) throw new EmptyTaskException();

        task.setTitle(title);
        task.setDescription(description);
        return task;
    }

    @NotNull
    @Override
    public Task updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String title,
            @Nullable final String description
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(id)) throw new EmptyIdException();
        if (ValidRuleUtil.isNullOrEmpty(title)) throw new EmptyTitleException();
        if (ValidRuleUtil.isNullOrEmpty(description)) throw new EmptyDescriptionException();

        @Nullable final Task task = getById(userId, id);
        if (Objects.isNull(task)) throw new EmptyTaskException();

        task.setTitle(title);
        task.setDescription(description);
        return task;
    }

    @Nullable
    @Override
    public @NotNull Task deleteByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(index)) throw new IllegalIndexException();
        return this.taskRepository.removeByIndex(userId, index);
    }

    @NotNull
    @Override
    public Task deleteById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        if (Objects.isNull(id)) throw new EmptyIdException();
        return this.taskRepository.removeById(userId, id);
    }

    @NotNull
    @Override
    public Task deleteByTitle(
            @Nullable final String userId,
            @Nullable final String title
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        if (Objects.isNull(title)) throw new EmptyTitleException();
        return this.taskRepository.removeByTitle(userId, title);
    }

    @NotNull
    @Override
    public Collection<Task> deleteAll(@Nullable final String userId) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        return this.taskRepository.removeAll(userId);
    }

    @Nullable
    @Override
    public Task getByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(index)) throw new IllegalIndexException();
        return this.taskRepository.getByIndex(userId, index);
    }

    @Nullable
    @Override
    public Task getById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        if (Objects.isNull(id)) throw new EmptyIdException();
        return this.taskRepository.getById(userId, id);
    }

    @Nullable
    @Override
    public Task getByTitle(
            @Nullable final String userId,
            @Nullable final String title
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        if (Objects.isNull(title)) throw new EmptyTitleException();
        return this.taskRepository.getByTitle(userId, title);
    }

    @NotNull
    @Override
    public Collection<Task> getAll(@Nullable final String userId) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        return this.taskRepository.getAll(userId);
    }

    @NotNull
    @Override
    public Collection<Task> initDemoData(@Nullable final Collection<User> users) {
        if (Objects.isNull(users)) throw new EmptyUserException();

        @NotNull final List<Task> assignedData = new ArrayList<>();
        users.forEach(user -> {
            @NotNull final Task task =
                    add(user.getId(), DataConst.TASK_TITLE, DataConst.TASK_DESCRIPTION);
            assignedData.add(task);
        });

        return assignedData;
    }

    @Nullable
    @Override
    public Collection<Task> importData(@Nullable final Collection<Task> tasks) {
        if (ValidRuleUtil.isNullOrEmpty(tasks)) return null;
        return this.taskRepository.importData(tasks);
    }

    @NotNull
    @Override
    public Collection<Task> exportData() {
        return this.taskRepository.exportData();
    }

}