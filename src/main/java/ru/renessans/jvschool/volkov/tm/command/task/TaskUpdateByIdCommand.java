package ru.renessans.jvschool.volkov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.service.IAuthenticationService;
import ru.renessans.jvschool.volkov.tm.api.service.IOwnerService;
import ru.renessans.jvschool.volkov.tm.model.Task;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

public final class TaskUpdateByIdCommand extends AbstractTaskCommand {

    @NotNull
    private static final String CMD_TASK_UPDATE_BY_ID = "task-update-by-id";

    @NotNull
    private static final String DESC_TASK_UPDATE_BY_ID = "обновить задачу по идентификатору";

    @NotNull
    private static final String NOTIFY_TASK_UPDATE_BY_ID =
            "Для обновления задачи по идентификатору введите идентификатор задачи из списка.\n" +
                    "Для обновления задачи введите её заголовок или заголовок с описанием.\n";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_TASK_UPDATE_BY_ID;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_TASK_UPDATE_BY_ID;
    }

    @Override
    public void execute() {
        ViewUtil.print(NOTIFY_TASK_UPDATE_BY_ID);
        @NotNull final String id = ViewUtil.getLine();
        @NotNull final String title = ViewUtil.getLine();
        @NotNull final String description = ViewUtil.getLine();
        @NotNull final IAuthenticationService authService = super.serviceLocator.getAuthenticationService();
        @Nullable final String userId = authService.getUserId();
        @NotNull final IOwnerService<Task> taskService = super.serviceLocator.getTaskService();
        @Nullable final Task updatedTask = taskService.updateById(userId, id, title, description);
        ViewUtil.print(updatedTask);
    }

}