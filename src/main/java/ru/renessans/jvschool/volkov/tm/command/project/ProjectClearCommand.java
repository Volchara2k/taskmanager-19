package ru.renessans.jvschool.volkov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.service.IAuthenticationService;
import ru.renessans.jvschool.volkov.tm.api.service.IOwnerService;
import ru.renessans.jvschool.volkov.tm.model.Project;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

import java.util.Collection;

public final class ProjectClearCommand extends AbstractProjectCommand {

    @NotNull
    private static final String CMD_PROJECT_CLEAR = "project-clear";

    @NotNull
    private static final String DESC_PROJECT_CLEAR = "очистить все проекты";

    @NotNull
    private static final String NOTIFY_PROJECT_CLEAR = "Производится очистка списка проекты";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_PROJECT_CLEAR;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_PROJECT_CLEAR;
    }

    @Override
    public void execute() {
        ViewUtil.print(NOTIFY_PROJECT_CLEAR);
        @NotNull final IAuthenticationService authService = super.serviceLocator.getAuthenticationService();
        @Nullable final String userId = authService.getUserId();
        @NotNull final IOwnerService<Project> projectService = super.serviceLocator.getProjectService();
        @Nullable final Collection<Project> projects = projectService.deleteAll(userId);
        ViewUtil.print(projects);
    }

}