package ru.renessans.jvschool.volkov.tm.service;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.repository.IUserRepository;
import ru.renessans.jvschool.volkov.tm.api.service.IUserService;
import ru.renessans.jvschool.volkov.tm.constant.DataConst;
import ru.renessans.jvschool.volkov.tm.enumeration.UserRole;
import ru.renessans.jvschool.volkov.tm.exception.empty.user.*;
import ru.renessans.jvschool.volkov.tm.exception.security.AccessFailureException;
import ru.renessans.jvschool.volkov.tm.model.User;
import ru.renessans.jvschool.volkov.tm.util.HashUtil;
import ru.renessans.jvschool.volkov.tm.util.ValidRuleUtil;

import java.util.*;

@AllArgsConstructor
public final class UserService implements IUserService {

    @NotNull
    private final IUserRepository userRepository;

    @Nullable
    @Override
    public User getUserById(@Nullable final String id) {
        if (ValidRuleUtil.isNullOrEmpty(id)) throw new EmptyUserIdException();
        return this.userRepository.getById(id);
    }

    @Nullable
    @Override
    public User getUserByLogin(@Nullable final String login) {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new EmptyLoginException();
        return this.userRepository.getByLogin(login);
    }

    @NotNull
    @Override
    public Collection<User> getExportData() {
        return this.userRepository.exportData();
    }

    @Nullable
    @Override
    public User addUser(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new EmptyLoginException();
        if (ValidRuleUtil.isNullOrEmpty(password)) throw new EmptyPasswordException();

        @NotNull final String passwordHash = HashUtil.saltHashLine(password);
        @NotNull final User user = new User(login, passwordHash);
        return this.userRepository.add(user);
    }

    @Nullable
    @Override
    public User addUser(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String firstName
    ) {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new EmptyLoginException();
        if (ValidRuleUtil.isNullOrEmpty(password)) throw new EmptyPasswordException();
        if (ValidRuleUtil.isNullOrEmpty(firstName)) throw new EmptyEmailException();

        @NotNull final String passwordHash = HashUtil.saltHashLine(password);
        @NotNull final User user = new User(login, passwordHash, firstName);
        return this.userRepository.add(user);
    }

    @Nullable
    @Override
    public User addUser(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final UserRole role
    ) {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new EmptyLoginException();
        if (ValidRuleUtil.isNullOrEmpty(password)) throw new EmptyPasswordException();
        if (Objects.isNull(role)) throw new EmptyUserRoleException();

        @NotNull final String passwordHash = HashUtil.saltHashLine(password);
        @NotNull final User user = new User(login, passwordHash, role);
        return this.userRepository.add(user);
    }

    @NotNull
    @Override
    public User updatePasswordById(
            @Nullable final String id,
            @Nullable final String newPassword
    ) {
        if (ValidRuleUtil.isNullOrEmpty(id)) throw new EmptyUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(newPassword)) throw new EmptyPasswordException();

        @NotNull final String passwordHash = HashUtil.saltHashLine(newPassword);
        @Nullable final User user = this.userRepository.getById(id);
        if (Objects.isNull(user)) throw new EmptyUserException();

        user.setPasswordHash(passwordHash);
        return user;
    }

    @Override
    public @NotNull User editProfileById(
            @Nullable final String id,
            @Nullable final String firstName
    ) {
        if (ValidRuleUtil.isNullOrEmpty(id)) throw new EmptyUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(firstName)) throw new EmptyFirstNameException();

        @Nullable final User user = this.userRepository.getById(id);
        if (Objects.isNull(user)) throw new EmptyUserException();

        user.setFirstName(firstName);
        return user;
    }

    @NotNull
    @Override
    public User editProfileById(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName
    ) {
        if (ValidRuleUtil.isNullOrEmpty(id)) throw new EmptyUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(firstName)) throw new EmptyFirstNameException();
        if (ValidRuleUtil.isNullOrEmpty(lastName)) throw new EmptyLastNameException();

        @Nullable final User user = this.userRepository.getById(id);
        if (Objects.isNull(user)) throw new EmptyUserException();

        user.setFirstName(firstName);
        user.setLastName(lastName);
        return user;
    }

    @NotNull
    @Override
    public User lockUserByLogin(@Nullable final String login) {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new EmptyLoginException();

        @Nullable final User user = getUserByLogin(login);
        if (Objects.isNull(user)) throw new EmptyUserException();
        if (user.getRole().isAdmin()) throw new AccessFailureException("Операция недоступна!");

        user.setLockdown(true);
        return user;
    }

    @NotNull
    @Override
    public User unlockUserByLogin(@Nullable final String login) {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new EmptyLoginException();

        @Nullable final User user = getUserByLogin(login);
        if (Objects.isNull(user)) throw new EmptyUserException();
        if (user.getRole().isAdmin()) throw new AccessFailureException("Операция недоступна!");

        user.setLockdown(false);
        return user;
    }

    @Nullable
    @Override
    public User deleteUserById(@Nullable final String id) {
        if (ValidRuleUtil.isNullOrEmpty(id)) throw new EmptyUserIdException();

        @Nullable final User user = getUserById(id);
        if (Objects.isNull(user)) throw new EmptyUserException();
        if (user.getRole().isAdmin()) throw new AccessFailureException("Операция недоступна!");

        return this.userRepository.deleteById(id);
    }

    @Nullable
    @Override
    public User deleteUserByLogin(@Nullable final String login) {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new EmptyLoginException();

        @Nullable final User user = getUserByLogin(login);
        if (Objects.isNull(user)) throw new EmptyUserException();
        if (user.getRole().isAdmin()) throw new AccessFailureException("Операция недоступна!");

        return this.userRepository.deleteByLogin(login);
    }

    @Nullable
    @Override
    public User deleteByUser(@Nullable final User user) {
        if (Objects.isNull(user)) throw new EmptyUserException();
        if (user.getRole().isAdmin()) throw new AccessFailureException("Операция недоступна!");
        return this.userRepository.deleteByUser(user);
    }

    @NotNull
    @Override
    public Collection<User> initDemoData() {
        return Arrays.asList(
                addUser(DataConst.USER_TEST_LOGIN, DataConst.USER_TEST_PASSWORD),
                addUser(DataConst.USER_DEFAULT_LOGIN, DataConst.USER_DEFAULT_PASSWORD,
                        DataConst.USER_DEFAULT_FIRSTNAME),
                addUser(DataConst.USER_ADMIN_LOGIN, DataConst.USER_ADMIN_PASSWORD, UserRole.ADMIN)
        );
    }

    @Nullable
    @Override
    public Collection<User> importData(@Nullable final Collection<User> userList) {
        if (ValidRuleUtil.isNullOrEmpty(userList)) return null;
        return this.userRepository.importData(userList);
    }

}