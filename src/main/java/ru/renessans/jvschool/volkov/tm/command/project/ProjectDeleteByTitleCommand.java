package ru.renessans.jvschool.volkov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.service.IAuthenticationService;
import ru.renessans.jvschool.volkov.tm.api.service.IOwnerService;
import ru.renessans.jvschool.volkov.tm.model.Project;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

public final class ProjectDeleteByTitleCommand extends AbstractProjectCommand {

    @NotNull
    private static final String CMD_PROJECT_DELETE_BY_TITLE = "project-delete-by-title";

    @NotNull
    private static final String DESC_PROJECT_DELETE_BY_TITLE = "удалить проект по заголовку";

    @NotNull
    private static final String NOTIFY_PROJECT_DELETE_BY_TITLE =
            "Для удаления заголовка по имени введите заголовок проекта из списка.\n";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_PROJECT_DELETE_BY_TITLE;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_PROJECT_DELETE_BY_TITLE;
    }

    @Override
    public void execute() {
        ViewUtil.print(NOTIFY_PROJECT_DELETE_BY_TITLE);
        @NotNull final String title = ViewUtil.getLine();
        @NotNull final IAuthenticationService authService = super.serviceLocator.getAuthenticationService();
        @Nullable final String userId = authService.getUserId();
        @NotNull final IOwnerService<Project> projectService = super.serviceLocator.getProjectService();
        @Nullable final Project project = projectService.deleteByTitle(userId, title);
        ViewUtil.print(project);
    }

}