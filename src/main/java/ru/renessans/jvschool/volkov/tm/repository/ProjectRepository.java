package ru.renessans.jvschool.volkov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.repository.IOwnerRepository;
import ru.renessans.jvschool.volkov.tm.exception.empty.owner.EmptyProjectException;
import ru.renessans.jvschool.volkov.tm.model.Project;

import java.util.*;
import java.util.stream.Collectors;

public final class ProjectRepository implements IOwnerRepository<Project> {

    @NotNull
    private final Map<String, Project> projectMap = new LinkedHashMap<>();

    @NotNull
    @Override
    public Project add(
            @NotNull final String userId,
            @NotNull final Project project
    ) {
        project.setUserId(userId);
        this.projectMap.put(project.getId(), project);
        return project;
    }

    @NotNull
    @Override
    public Project removeByIndex(
            @NotNull final String userId,
            @NotNull final Integer index
    ) {
        final Project project = getByIndex(userId, index);
        if (Objects.isNull(project)) throw new EmptyProjectException();
        return remove(userId, project);
    }

    @NotNull
    @Override
    public Project removeById(
            @NotNull final String userId,
            @NotNull final String id
    ) {
        @Nullable final Project project = getById(userId, id);
        if (Objects.isNull(project)) throw new EmptyProjectException();
        return remove(userId, project);
    }

    @NotNull
    @Override
    public Project removeByTitle(
            @NotNull final String userId,
            @NotNull final String title
    ) {
        @Nullable final Project project = getByTitle(userId, title);
        if (Objects.isNull(project)) throw new EmptyProjectException();
        return remove(userId, project);
    }

    @Nullable
    @Override
    public @NotNull Project remove(
            @NotNull final String userId,
            @NotNull final Project project
    ) {
        this.projectMap.remove(project.getId());
        return project;
    }

    @NotNull
    @Override
    public Collection<Project> removeAll(@NotNull final String userId) {
        @NotNull final List<Project> removedProjects = new ArrayList<>(getAll(userId));
        this.projectMap.entrySet().removeIf(taskEntry ->
                userId.equals(taskEntry.getValue().getUserId()));
        return removedProjects;
    }

    @Nullable
    @Override
    public Project getByIndex(
            @NotNull final String userId,
            @NotNull final Integer index
    ) {
        @NotNull final List<Project> userProjects = new ArrayList<>(getAll(userId));
        return userProjects.get(index);
    }

    @Nullable
    @Override
    public Project getById(
            @NotNull final String userId,
            @NotNull final String id
    ) {
        return getAll(userId)
                .stream()
                .filter(task -> id.equals(task.getId()))
                .findAny()
                .orElse(null);
    }

    @Nullable
    @Override
    public Project getByTitle(
            @NotNull final String userId,
            @NotNull final String title
    ) {
        return getAll(userId)
                .stream()
                .filter(task -> title.equals(task.getTitle()))
                .findAny()
                .orElse(null);
    }

    @NotNull
    @Override
    public Collection<Project> getAll(@NotNull final String userId) {
        return exportData()
                .stream()
                .filter(task -> userId.equals(task.getUserId()))
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public Collection<Project> importData(@NotNull final Collection<Project> projects) {
        clearData();
        projects.forEach(project -> add(project.getUserId(), project));
        return exportData();
    }

    @Override
    public void clearData() {
        this.projectMap.clear();
    }

    @NotNull
    @Override
    public Collection<Project> exportData() {
        return new ArrayList<>(this.projectMap.values());
    }

}