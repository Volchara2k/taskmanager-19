package ru.renessans.jvschool.volkov.tm.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public final class Project extends AbstractSerializableModel {

    private static final long serialVersionUID = 1L;

    @NotNull
    private String title = "";

    @NotNull
    private String description = "";

    @NotNull
    private String userId;

    public Project(@NotNull final String title) {
        this.title = title;
    }

    public Project(
            @NotNull final String title,
            @NotNull final String description
    ) {
        this.title = title;
        this.description = description;
    }

    @NotNull
    @Override
    public String toString() {
        return "Заголовок проекта: " + this.title +
                ", описание проекта: " + this.description +
                "\nИдентификатор: " + super.id + "\n";
    }

}