package ru.renessans.jvschool.volkov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.service.IAuthenticationService;
import ru.renessans.jvschool.volkov.tm.api.service.IOwnerService;
import ru.renessans.jvschool.volkov.tm.model.Task;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

public final class TaskDeleteByIndexCommand extends AbstractTaskCommand {

    @NotNull
    private static final String CMD_TASK_DELETE_BY_INDEX = "task-delete-by-index";

    @NotNull
    private static final String DESC_TASK_DELETE_BY_INDEX = "удалить задачу по индексу";

    @NotNull
    private static final String NOTIFY_TASK_DELETE_BY_INDEX =
            "Для удаления задачи по индексу введите индекс задачи из списка ниже.\n";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_TASK_DELETE_BY_INDEX;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_TASK_DELETE_BY_INDEX;
    }

    @Override
    public void execute() {
        ViewUtil.print(NOTIFY_TASK_DELETE_BY_INDEX);
        @NotNull final Integer index = ViewUtil.getInteger() - 1;
        @NotNull final IAuthenticationService authService = super.serviceLocator.getAuthenticationService();
        @Nullable final String userId = authService.getUserId();
        @NotNull final IOwnerService<Task> taskService = super.serviceLocator.getTaskService();
        @Nullable final Task task = taskService.deleteByIndex(userId, index);
        ViewUtil.print(task);
    }

}