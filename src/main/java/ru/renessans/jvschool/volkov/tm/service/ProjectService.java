package ru.renessans.jvschool.volkov.tm.service;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.repository.IOwnerRepository;
import ru.renessans.jvschool.volkov.tm.api.service.IOwnerService;
import ru.renessans.jvschool.volkov.tm.constant.DataConst;
import ru.renessans.jvschool.volkov.tm.exception.empty.owner.EmptyDescriptionException;
import ru.renessans.jvschool.volkov.tm.exception.empty.owner.EmptyIdException;
import ru.renessans.jvschool.volkov.tm.exception.empty.owner.EmptyProjectException;
import ru.renessans.jvschool.volkov.tm.exception.empty.owner.EmptyTitleException;
import ru.renessans.jvschool.volkov.tm.exception.empty.user.EmptyUserException;
import ru.renessans.jvschool.volkov.tm.exception.empty.user.EmptyUserIdException;
import ru.renessans.jvschool.volkov.tm.exception.illegal.IllegalIndexException;
import ru.renessans.jvschool.volkov.tm.model.Project;
import ru.renessans.jvschool.volkov.tm.model.User;
import ru.renessans.jvschool.volkov.tm.util.ValidRuleUtil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

@AllArgsConstructor
public final class ProjectService implements IOwnerService<Project> {

    @NotNull
    private final IOwnerRepository<Project> projectRepository;

    @NotNull
    @Override
    public Project add(
            @Nullable final String userId,
            @Nullable final String title,
            @Nullable final String description
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(title)) throw new EmptyTitleException();
        if (ValidRuleUtil.isNullOrEmpty(description)) throw new EmptyDescriptionException();

        @NotNull final Project project = new Project(title, description);
        return this.projectRepository.add(userId, project);
    }

    @NotNull
    @Override
    public Project updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String title,
            @Nullable final String description
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(index)) throw new IllegalIndexException();
        if (ValidRuleUtil.isNullOrEmpty(title)) throw new EmptyTitleException();
        if (ValidRuleUtil.isNullOrEmpty(description)) throw new EmptyDescriptionException();

        @Nullable final Project project = getByIndex(userId, index);
        if (Objects.isNull(project)) throw new EmptyProjectException();

        project.setTitle(title);
        project.setDescription(description);
        return project;
    }

    @NotNull
    @Override
    public Project updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String title,
            @Nullable final String description
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(id)) throw new EmptyIdException();
        if (ValidRuleUtil.isNullOrEmpty(title)) throw new EmptyTitleException();
        if (ValidRuleUtil.isNullOrEmpty(description)) throw new EmptyDescriptionException();

        @Nullable final Project project = getById(userId, id);
        if (Objects.isNull(project)) throw new EmptyProjectException();

        project.setTitle(title);
        project.setDescription(description);
        return project;
    }

    @NotNull
    @Override
    public Project deleteByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(index)) throw new IllegalIndexException();
        return this.projectRepository.removeByIndex(userId, index);
    }

    @NotNull
    @Override
    public  Project deleteById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        if (Objects.isNull(id)) throw new EmptyIdException();
        return this.projectRepository.removeById(userId, id);
    }

    @NotNull
    @Override
    public Project deleteByTitle(
            @Nullable final String userId,
            @Nullable final String title
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        if (Objects.isNull(title)) throw new EmptyTitleException();
        return this.projectRepository.removeByTitle(userId, title);
    }

    @NotNull
    @Override
    public Collection<Project> deleteAll(final String userId) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        return this.projectRepository.removeAll(userId);
    }

    @Nullable
    @Override
    public Project getByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(index)) throw new IllegalIndexException();
        return this.projectRepository.getByIndex(userId, index);
    }

    @Nullable
    @Override
    public Project getById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        if (Objects.isNull(id)) throw new EmptyIdException();
        return this.projectRepository.getById(userId, id);
    }

    @Nullable
    @Override
    public Project getByTitle(
            @Nullable final String userId,
            @Nullable final String title
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        if (Objects.isNull(title)) throw new EmptyTitleException();
        return this.projectRepository.getByTitle(userId, title);
    }

    @NotNull
    @Override
    public Collection<Project> getAll(@Nullable final String userId) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        return this.projectRepository.getAll(userId);
    }

    @NotNull
    @Override
    public Collection<Project> initDemoData(@Nullable final Collection<User> users) {
        if (Objects.isNull(users)) throw new EmptyUserException();

        @NotNull final List<Project> assignedData = new ArrayList<>();
        users.forEach(user -> {
            @NotNull final Project project =
                    add(user.getId(), DataConst.PROJECT_TITLE, DataConst.PROJECT_DESCRIPTION);
            assignedData.add(project);
        });

        return assignedData;
    }

    @Nullable
    @Override
    public Collection<Project> importData(@Nullable final Collection<Project> projects) {
        if (ValidRuleUtil.isNullOrEmpty(projects)) return null;
        return this.projectRepository.importData(projects);
    }

    @Nullable
    @Override
    public @NotNull Collection<Project> exportData() {
        return this.projectRepository.exportData();
    }

}