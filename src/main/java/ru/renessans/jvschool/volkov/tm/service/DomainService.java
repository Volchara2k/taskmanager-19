package ru.renessans.jvschool.volkov.tm.service;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.service.IOwnerService;
import ru.renessans.jvschool.volkov.tm.api.service.IDomainService;
import ru.renessans.jvschool.volkov.tm.api.service.IUserService;
import ru.renessans.jvschool.volkov.tm.dto.Domain;
import ru.renessans.jvschool.volkov.tm.exception.empty.data.EmptyDomainException;
import ru.renessans.jvschool.volkov.tm.model.Project;
import ru.renessans.jvschool.volkov.tm.model.Task;
import java.util.Objects;

@AllArgsConstructor
public final class DomainService implements IDomainService {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IOwnerService<Task> taskService;

    @NotNull
    private final IOwnerService<Project> projectService;

    @Override
    public void dataImport(@Nullable final Domain domain) {
        if (Objects.isNull(domain)) throw new EmptyDomainException();
        this.userService.importData(domain.getUsers());
        this.taskService.importData(domain.getTasks());
        this.projectService.importData(domain.getProjects());
    }

    @Override
    public void dataExport(@Nullable final Domain domain) {
        if (Objects.isNull(domain)) throw new EmptyDomainException();
        domain.setUsers(this.userService.getExportData());
        domain.setTasks(this.taskService.exportData());
        domain.setProjects(this.projectService.exportData());
    }

}