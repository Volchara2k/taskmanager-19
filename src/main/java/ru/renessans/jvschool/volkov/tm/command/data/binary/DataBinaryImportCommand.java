package ru.renessans.jvschool.volkov.tm.command.data.binary;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.service.IDomainService;
import ru.renessans.jvschool.volkov.tm.command.data.AbstractDataCommand;
import ru.renessans.jvschool.volkov.tm.dto.Domain;
import ru.renessans.jvschool.volkov.tm.util.DataInterChangeUtil;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

import java.util.Objects;

public final class DataBinaryImportCommand extends AbstractDataCommand {

    @NotNull
    private static final String CMD_BIN_IMPORT = "data-bin-import";

    @NotNull
    private static final String DESC_BIN_IMPORT = "импортировать домен из бинарного вида";

    @NotNull
    private static final String NOTIFY_BIN_IMPORT = "Происходит процесс загрузки домена из бинарного вида...";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_BIN_IMPORT;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_BIN_IMPORT;
    }

    @Override
    public void execute() throws Exception {
        ViewUtil.print(NOTIFY_BIN_IMPORT);

        @Nullable final Domain domain;
        try (@NotNull final DataInterChangeUtil dataInterchange = new DataInterChangeUtil()){
            domain = dataInterchange.readFromBin(BIN_FILE_LOCATE, Domain.class);
        }

        if (Objects.isNull(domain)) return;
        @NotNull final IDomainService domainService = super.serviceLocator.getDomainService();
        domainService.dataImport(domain);

        ViewUtil.print(domain.getUsers());
        ViewUtil.print(domain.getTasks());
        ViewUtil.print(domain.getProjects());
    }

}